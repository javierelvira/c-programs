#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100
#define N 5

struct Empleado{
    char nombre[MAX];
    double sueldo;
};

int main(int argc, char *argv[]){

    struct Empleado jefe;
    struct Empleado empleado[N];

    //Almacenamos los datos de la variable
    for (int cnt=0; cnt<N; cnt++)
    {
        printf ("Nombre: ");
        scanf(" %s", empleado[cnt].nombre);
        printf("Sueldo: ");
        scanf(" %lf", &empleado[cnt].sueldo);
    }
    //
    for (int i=0; i<N; i++)
    printf("%s: => %.2lf€\n", empleado[i].nombre, empleado[i].sueldo);


    return EXIT_SUCCESS;
}
