#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define N 12
#define ARCHIVO "fibobinario.dat"

int main(int argc, char *argv[]){

    int numeros[N] = {1,1,2,3,5,8,13,21,34,55,89,144};
    FILE *pf = NULL;

    if ( !(pf = fopen ( ARCHIVO, "wb")))
        return 1;
    fwrite(numeros,sizeof(int), 12,pf);
    fclose (pf);


    return EXIT_SUCCESS;
}
