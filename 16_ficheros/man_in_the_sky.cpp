#include <stdio.h>
#include <stdlib.h>

#define Nombre "cancion.txt"

    //song contiene la direccion de memoria del salto de linea
    const char * song = "\n\
                     I believe for every drop of rain that falls\n\
                     A flower grows\n\
                     I believe that somewhere in the darkest night\n\
                     A candle glows\n\
                     I believe for everyone who goes astray, someone will come\n\
                     To show the way\n\
                     I believe, I believe\n\
                     I believe above a storm the smallest prayer\n\
                     Can still be heard\n\
                     I believe that someone in the great somewhere\n\
                     Hears every word\n\
                     Everytime I hear a new born baby cry,\n\
                     Or touch a leaf or see the sky\n\
                     Then I know why, I believe\n\
                     Everytime I hear a new born baby cry,\n\
                     Or touch a leaf or see the sky\n\
                     Then I know why, I believe\n\
                     ";

void print_usage (){
    printf("Esto se usa así\n");
}

void informo(const char *mssg){
    print_usage ();
    fprintf(stderr, "%s\n", mssg);
    exit (1);
}

int main(int argc, char *argv[]){

    FILE *fichero;

    if ( !(fichero = fopen ( Nombre, "w")))
        informo("No se ha podido abrir el archivo");
    fprintf(fichero, "%s\n", song);
    fclose (fichero);

    return EXIT_SUCCESS;
}
