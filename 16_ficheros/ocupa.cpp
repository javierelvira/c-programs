#include <stdio.h>
#include <stdlib.h>

#define FILENAME "nombres.txt"

int main(int argc, char *argv[]){
    FILE *pf;

    if ( !(pf = fopen (FILENAME, "r")) ){
      fprintf (stderr, "Ain.\n");
      return EXIT_FAILURE;
    }
    ftell(pf);
    printf("Ocupa %i bytes\n", (char) getc (pf));

    fclose(pf);

    return EXIT_SUCCESS;
}
