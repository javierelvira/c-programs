#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define FILENAME "nombres.txt"

int main(int argc, char *argv[]){
    FILE *pf;
    int pos;

    srand (time (NULL));
    if ( !(pf= fopen (FILENAME, "r")) ){
      fprintf (stderr, "Ain.\n");
     return EXIT_FAILURE;
    }
    pos = rand () % 100;

    fseek (pf, pos, SEEK_SET); //SEEK_SET SEEK_CUR SEEK_END
    printf("Pos %i => %c\n", pos, (char) getc (pf));

    fclose (pf);

    return EXIT_SUCCESS;
}
