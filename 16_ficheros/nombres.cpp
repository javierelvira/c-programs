#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#define MAX 100
#define N 10
#define ARCHIVO "nombres.txt"

int main(int argc, char *argv[]){

    char nombre[N][MAX];
    nombre[N][MAX] = 0;
    FILE *pf = NULL;
    int i=0;
    int a=0;

    for(int cnt=0; cnt<N; cnt++)
    {
      printf("Dime un nombre: ");
      fgets(nombre[cnt], MAX, stdin);
    }

    if ( !(pf = fopen ( ARCHIVO, "w")))
        return 1;

    for (int cnt = 0; cnt<N; cnt++)
        fprintf(pf, "%s", nombre[cnt]);
    fclose (pf);


    return EXIT_SUCCESS;
}
