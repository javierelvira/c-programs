#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TEXT "fichero.txt"
#define NUMLETRAS ('z' - 'a' + 1)

int main(int argc, char *argv[]){
    long inicio, fin;
    int total = 0;
    char texto;
    FILE *pf;
    unsigned freq[NUMLETRAS];
    bzero(freq, sizeof(freq));

    if ( !(pf = fopen (TEXT, "rb")) ){
          fprintf(stderr, "error\n");
          return EXIT_FAILURE;
    }

    inicio = ftell(pf);
    fseek(pf, 0, SEEK_END);
    fin = ftell(pf);
    rewind (pf); //vuelve a llevar el cursor al inicio del fichero

    texto = (char *) malloc (fin-inicio+1);
    fread(texto, sizeof(char), fin-inicio, pf);
    *(texto + fin - inicio) = '\0';

    for (const char *dedo = texto; *dedo != '\0'; dedo++, total++)
        freq[*dedo -'a']++;

    for (int c='a'; c<='z'; c++)
        printf("%c: %.2lf%%\n", c, (double)100. * freq[c - 'a'] / total);

    free(texto);
    fclose(pf);

    return EXIT_SUCCESS;
}
