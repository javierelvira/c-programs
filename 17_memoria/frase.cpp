#include <stdio.h>
#include <stdlib.h>
#include <string.h>


int main(int argc, char *argv[]){
    char *frase;
    char *p;

    printf("Dime una frase: ");
    scanf(" %m[^\n]", &frase);

    p = frase;

    printf("Frase codificada: ");
    while (*p != '\0'){
      printf("%c", *p+3);
      p++;
    }

    printf("\n");
    free(frase);

    return EXIT_SUCCESS;
}
