#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#define ARCH "espa~nol.words"
#define N 0x100

long random(long longfile){
    int number;

    srand(time(NULL));

    number = 1 + rand() % ((longfile+1) - 1);

    return number;
}


int main(int argc, char *argv[]){

    long inicio, fin;
    long longfile;
    FILE *pf;
    char *list;
    long randomnumb = 0;
    int beginword = 0;
    int endword = 0;
    int longword = 0;
    char word[N];
    int bg = 0;
    //Variables que utilizamos en los while para autoincrementar
    int i=0;
    int x=0;
    int a=0;

    //Abrimos archivo y ponemos un error en casod e que no se pueda abrir
    if( !(pf=fopen(ARCH, "rb")) ){
        fprintf(stderr,"ERROR");
        return EXIT_FAILURE;
    }

    //Medimos el archivo de esta forma
    inicio = ftell(pf);
    fseek(pf,0,SEEK_END);
    fin = ftell(pf);
    rewind(pf);

    longfile = fin - inicio;

    //Metemos el numero random(función de arriba) en una variable
    randomnumb = random(longfile);

    //Volcamos el archivo en el malloc para poder recortar las palabras
    list = (char*) malloc(longfile);
    fread(list ,sizeof(char *), longfile, pf);


    //Este primer while se encarga de llevar el puntero al comienzo de la palabra que vamos a recortar
    while(list[randomnumb+i++] != '>')
        beginword++;

    bg = randomnumb + beginword + 2;

    //Este segundo while calcula el tamaño de la palabra seleccionada
    while(list[bg+a++] != '>')
        longword++;

    //Añadimos caracter a caracter de la palabra en un array
    for(int cnt = bg; cnt < bg+longword; cnt++){
        word[x++] = list[cnt];
    }
    printf("\n");

    //Imprimimos el array donde se encuentra la palabra
    for(int cnt2=0; cnt2<longword; cnt2++)
        printf("%c", word[cnt2]);


    fclose(pf);

    return EXIT_SUCCESS;

}
