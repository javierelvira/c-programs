#include <stdio.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>

#define TEXT "fichero.txt"
#define N 300

int main(int argc, char *argv[]){

    FILE *pf;
    char palabra[N], *texto;
    int i = 0;
    long longfile, inicio, fin;
    int longword = 0;
    int w=0;

    if( !(pf= fopen(TEXT, "rb")) ){
        fprintf(stderr, "Archivo no valido");
        return EXIT_FAILURE;
    }

    inicio = ftell(pf);
    fseek(pf, 0, SEEK_END);
    fin = ftell(pf);
    rewind (pf);
    longfile = fin - inicio;

    texto = (char *) malloc(longfile);
    fread(texto, sizeof(char), longfile, pf);

    printf("Mete la palabra a buscar en fichero.txt: \n");
    scanf("%s",palabra);

    while(palabra[i++] != '\0')
        longword++;

    printf("Tu palabra es: %s\n", palabra);           //Print word search
    printf("Tu palabra mide %i caracteres\n", longword);            //Print length of word search
    printf("\n");

    for(int cnt=0; cnt < longfile; cnt++)
        printf("%c", texto[cnt]);

    printf("Tu texto tiene %li caracteres\n", longfile);
    printf("\n\n");

    for(int cnt2=0; cnt2<longfile; cnt2++){
        if (palabra[w] == texto[cnt2]){
            ++w;
            if(w == longword)
                printf("Tu palabra comienza con el caracter: %i\n", 2 + cnt2 -w);
        }
        else w=0;
    }

    free(texto);
    fclose(pf);

    return EXIT_SUCCESS;
}





