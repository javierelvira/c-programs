#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 2
#define PI 3.14

int main(){

    double vec[DIM],
           modulo,
           vec2[DIM],
           modulo2,
           valor,
           angulo;
    int prod;

    system ("clear");

    printf("Dime el primer vector: ");
    scanf(" %lf, %lf", &vec[0], &vec[1]);

    modulo = sqrt ( pow(vec[0], 2) + pow(vec[1], 2) );
    printf ("Modulo: %.2lf\n", modulo);

    printf("Dime el segundo vector: ");
    scanf(" %lf, %lf", &vec2[0], &vec2[1]);

    modulo2 = sqrt ( pow(vec2[0], 2) + pow(vec2[1], 2) );
    printf ("Modulo2: %.2lf\n", modulo2);

    prod = ( (vec[0] * vec2[1]) - (vec[1] * vec2[0]));
    printf("El producto vectorial es: i=0, j=0 k=%i\n", prod);

    angulo = asin( prod / (modulo * modulo2)) * 180 / PI;


    // printf("El ángulo es: %.2lfº\n", angulo);

     return EXIT_SUCCESS;
}
