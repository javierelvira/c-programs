#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 3
#define PI 3.14

int main(){

    int   mat1[DIM],
          mat2[DIM],
          mat3[DIM],
          det1,
	  det2,
	  det;

    system ("clear");

    printf("Dime la primera fila de la matriz: ");
    scanf(" %i %i %i", &mat1[0], &mat1[1], &mat1[2]);

    printf("Dime la segunda fila de la matriz: ");
    scanf(" %i %i %i", &mat2[0], &mat2[1], &mat2[2]);

    printf("Dime la tercera fila de la matriz: ");
    scanf(" %i %i %i", &mat3[0], &mat3[1], &mat3[2]);

    printf("La matriz quedaría así:\n%i %i %i\n",mat1[0], mat1[1], mat1[2]);
    printf("%i %i %i\n",mat2[0], mat2[1], mat2[2]);
    printf("%i %i %i\n",mat3[0], mat3[1], mat3[2]);

    det1=((mat1[0] * mat2[1] * mat3[2])+(mat2[0] * mat3[1] * mat1[2])+(mat3[0] * mat1[1] * mat2[2]));
    printf("El primer determinante de la matriz es: %i\n", det1);

    det2=((mat3[0] * mat2[1] * mat1[2])+(mat2[2] * mat3[1] * mat1[0])+(mat3[2] * mat1[1] * mat2[0]));
    printf("El segundo determinante de la matriz es: %i\n", det2);

    det=det1-det2;
    printf("El determinante final de la matriz es: %i\n", det);

    return EXIT_SUCCESS;
}
