#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 3
#define PI 3.14

int main(){

    int   mat1[DIM],
          mat2[DIM],
          mat3[DIM],
          det1,
	  det2;

    system ("clear");

    printf("Dime el primer vector: ");
    scanf(" %i %i %i", &mat1[0], &mat1[1], &mat1[2]);

    printf("Dime el segundo vector: ");
    scanf(" %i %i %i", &mat2[0], &mat2[1], &mat2[2]);

    printf("Dime el vector por el cual los quieres multiplicar: ");
    scanf(" %i %i %i", &mat3[0], &mat3[1], &mat3[2]);

    det1=((mat1[0] * mat3[0]) + (mat1[1] * mat3[1]) + (mat1[2] * mat3[2]));
    det2=((mat2[0] * mat3[0]) + (mat2[1] * mat3[1]) + (mat2[2] * mat3[2]));

    printf("Al multiplicar el primer vector sale: %i\n", det1);
    printf("Al multiplicar el segundo vector sale: %i\n", det2);

    return EXIT_SUCCESS;
}
