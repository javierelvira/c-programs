#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 2
#define PI 3.14

int main(){

    int   coor[DIM],
          angulo;
    double angulo2,
           coseno,
           seno,
           x1,
           x2,
           y1,
           y2;

    system ("clear");

    printf("Dime la coordenada: ");
    scanf(" %i %i", &coor[0], &coor[1]);

    printf("Dime el angulo que quieres rotar: ");
    scanf(" %i", &angulo);

    angulo2=(angulo * PI/180);

    seno=sin(angulo2);
    coseno=cos(angulo2);

    printf("%.2lf\n", coseno);
    printf("%.2lf\n", seno);

    x1=(coor[0] * cos(angulo2));
    x2=(coor[1] * sin(angulo2));

    y1=(coor[0] * (-1) * sin(angulo2));
    y2=(coor[1] * cos(angulo2));

    printf("La coordenada x sería: (%.2lf,%.2lf)\n", x1, x2);
    printf("La coordenada y sería: (%.2lf,%.2lf)\n", y1, y2);

    return EXIT_SUCCESS;
}
