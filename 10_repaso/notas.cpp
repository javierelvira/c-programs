#include <stdio.h>
#include <stdlib.h>

#define N 10

int main(){

    int lista[N];
    int nota;
    int resultado;
    int suma;

    for (int i=0; i<N; i++){
      lista[i] = 1 * (i + 1);
      printf("Dime la nota del alumno: ");
      scanf("%i", &nota);
      printf("\n");
    }

    for (int i=0; i<N; i++){
      suma += lista[i];
      resultado=lista[i]/N;
    }

    printf("La media de la clase es: %i", resultado);
    printf("\n");

    return EXIT_SUCCESS;
}
