#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define DIM 2
#define PI 3.14

int main(){

    double vec[DIM],
           tan,
           angulo;

    system ("clear");

    printf("Dime el vector: ");
    scanf(" %lf, %lf", &vec[0], &vec[1]);

    tan = vec[1] / vec[0];
    printf ("Tangente: %.2lf\n", tan);

    angulo = atan(tan) * 180 / PI;
    printf ("Angulo: %.2lf\n", angulo);

    //Con atan2 hace directamente la división y también se fija en el símbolo
    angulo = atan2(vec[1], vec[0]) * 180 / PI;
    printf ("Angulo: %.2lf\n", angulo);

    return EXIT_SUCCESS;
}
