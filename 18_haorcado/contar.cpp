#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>
#include <string.h>

#define GREEN_ON "\x1B[32m"
#define RESET "\x1B[0m"
#define RED_ON "\x1B[31m"

#define VIDAS 10
#define MAX 0x200

bool es_mala (char letra){
    return true;
}

int main(int argc, char *argv[]){
    char *malas, *letras;
    char letra;
    int total_malas = 0, total_letras = 0;

    malas = (char *) malloc (VIDAS + 1);
    letras = (char *) malloc (MAX + 1);
    bzero (malas, VIDAS + 1);
    bzero (letras, MAX + 1);
    //memset(malas, 0, VIDAS) poner a 0 la variable

    while (total_malas < VIDAS){
        system("clear");
        system("toilet -f pagga --metal HAORCADO");
        printf("Letras: " GREEN_ON "%s" RESET "\n", letras); // \x1B es una secuencia de escape y [32m es para cambiar el color
        printf("Malas: " RED_ON "%s" RESET "\n", malas);
        printf("---------------------------------\n\n");

        printf("Dime una letra: ");
        letra = getchar ();
        __fpurge (stdin);
        if(strchr (letras, letra))
            continue;

        if (es_mala (letra))
            *(malas + total_malas++) = letra;
        *(letras + total_letras++) = letra;
    }

    free(letras);
    free(malas);

    return EXIT_SUCCESS;
}
