#include <stdio.h>
#include <allegro5/allegro.h> //libreria de allegro

int main(int argc, char **argv){

    ALLEGRO_DISPLAY *display = NULL;

    if(!al_init()) {
        fprintf(stderr, "Error al inicializar allegro!\n");
        return -1;
    }

    display = al_create_display(640, 480); //Tamaño del display que se va a mostrar
    if(!display) {
        fprintf(stderr, "Error al crear el display!\n");
        return -1;
    }

    al_clear_to_color(al_map_rgb(0,255,0)); //cambiar color de fondo

    al_flip_display();

    al_rest(5.0); //segundos que tarda en cerrarse

    al_destroy_display(display);

    return EXIT_SUCCESS;
}





