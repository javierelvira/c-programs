#include <stdio.h>
#include <stdlib.h>

int main(){

  unsigned primo[] = {2, 3, 5, 7, 11, 13, 17, 19, 23};
  unsigned elementos = (unsigned) sizeof(primo) / sizeof(int);
  unsigned *peeping = primo;
  char *tom = (char *) primo;

    printf("PRIMO: \n"
           "=====\n"
           "Localización (%p)\n"
           "Elementos: %u [%u .. %u]\n"
           "Tamaño: %lu bytes.\n \n",
	   primo,
	   elementos,
	   primo[0], primo[8],
           sizeof(primo));

    printf("Elemento nº0: %u\n", peeping[0]);
    printf("Elemento nº1: %u\n", peeping[1]);
    printf("Elemento nº0: %u\n", *peeping );
    printf("Elemento nº1: %u\n", *(peeping+1));
    printf("Tamaño: %lu bytes. \n", sizeof(peeping));
    printf("\n");

    for (int i=0; i<sizeof(primo); i++)
    printf("%2X", *(tom + i));
    printf("\n\n");

    return EXIT_SUCCESS;
}
