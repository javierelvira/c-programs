#include <stdio.h>
#include <stdlib.h>
#define N 1000001

int main(){

  int emento[N];

  emento[1] = emento[0] = 1;

  for(int i = 2; i<N; i++)
    emento[i] = emento[i-1] + emento[i-2];

  for (int i = 0; i<N; i++)
    printf(" %i", emento[i]);

  printf("\n");

  for (int i = 1; i<N; i++)
    printf(" %lf", (double) emento[i]/emento[i-1]);

  printf("\n");

  return EXIT_SUCCESS;
}
