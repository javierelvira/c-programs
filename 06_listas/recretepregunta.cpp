#include <stdio.h>
#include <stdlib.h>

int main(){

  char respuesta;

  do {
      printf("¿Quieres que te cuente un cuento\n"
              "recuento que nunca se acaba? (s/n)");
      scanf("%c", &respuesta);
      if (respuesta != 'n')
      printf("Yo no te digo ni que si ni que no,\n"
              "lo que digo es que si ");
   } while( respuesta != 'n');

  printf("Como no hay que ser un pesado en esta vida: ADIOS, Isabel");

  return EXIT_SUCCESS;
}
