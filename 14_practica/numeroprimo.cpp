#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada

bool es_primo(int posibleprimo) {
    bool primo = true;

    for (int divisor=posibleprimo/2; divisor>1; divisor--)
          if (posibleprimo % divisor == 0)
                primo= false;

    return primo;
}

int main() {
    int numero;

    printf("Dime el número: ");
    scanf("%i", &numero);
    printf("%s es primo el %i.\n",es_primo(numero)? "Si": "No",numero);

    /*int respuesta, a;

    a=0;

    printf("Es primo el numero que quieres: ");
    scanf(" %d", &respuesta);

    for (int i=1; i<=respuesta; i++){
        if (respuesta%i==0)
            a++; //Saca el número total de divisores
    }

    if (a==2){ //solo puede tener dos divisores para ser primo
        printf ("True\n");
    }else{
        printf ("false\n");
    }
    */
    return EXIT_SUCCESS;
}

