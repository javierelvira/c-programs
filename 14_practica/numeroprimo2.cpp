#include <stdio.h>
#include <stdlib.h>

//Funcion punto de entrada

char const *programa_name; //const hace que sea una variable constante, si se pone antes del asteriscoel carácter es constante y no puede ser cambiado, si se pone después del asterisco, lo que no puede cambiar es la dirección de memoria.También se puede poner antes y depues del carácter para que no cambien ninguna de las dos.
void print_usage (int exit_code){
    FILE *f = stdout;
    if (exit_code !=0)
        f = stderr;
    fprintf(f,
            "Checks for primality of the argument.- \n\n"
            "Usage:   %s <number>\n\n"
            "Number: Positive integer.\n"
            "\n"
            , programa_name);
    exit (exit_code);
}

bool es_primo(int posibleprimo) {
    bool primo = true;

    for (int divisor=posibleprimo/2; divisor>1; divisor--)
          if (posibleprimo % divisor == 0)
                primo= false;

    return primo;
}

int main(int argc, char *argv[]) {
    programa_name = argv[0];
    if (argc <2)
        print_usage(1);

    int numero = atoi (argv[1]);

    printf("%s es primo el %i.\n",es_primo(numero)? "Si": "No",numero);

    /*int respuesta, a;

    a=0;

    printf("Es primo el numero que quieres: ");
    scanf(" %d", &respuesta);

    for (int i=1; i<=respuesta; i++){
        if (respuesta%i==0)
            a++; //Saca el número total de divisores
    }

    if (a==2){ //solo puede tener dos divisores para ser primo
        printf ("True\n");
    }else{
        printf ("false\n");
    }
    */
    return EXIT_SUCCESS;
}

