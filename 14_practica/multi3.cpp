#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define M 2
#define K 4
#define N 3

void imprime (double m[M][N], int f) {
    for (int i=0; i<f; i++){
        for (int j=0; j<N; j++)
            printf ("\t%7.2lf", m[i][j]);
        printf ("\n");
    }
}

int main(){

    double A[M][K] = {
        {2, 3, 5, 1},
        {3, 1, 4, 2}
    },
           B[K][N] = {
               {5,  2, 1},
               {3, -7, 2},
               {-4, 5, 1},
               {2, 3, -9}
           },
           C[M][N];

    for (int i=0; i<M; i++)
        for(int j=0; j<N; j++){
            C[i][j] = 0;
            for (int k=0; k<K; k++)
                C[i][j] += A[i][k] * B[k][j];
        }

    // Se podrían usar los "printf" de abajo en vez de la función "imprime".

    // printf("{%2.lf %2.lf %2.lf}\n", C[0][0], C[0][1], C[0][2]);
    // printf("{%2.lf %2.lf %2.lf}\n", C[1][0], C[1][1], C[1][2]);

    imprime (C, 2);
    printf("\n");
    imprime (B, 4);

    return EXIT_SUCCESS;
}

