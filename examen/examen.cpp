#include <stdio.h>
#include <stdlib.h>
#include "struct.h"

#define file "fichero.txt"
#define MAX 0x100


void rellenaDatos(struct TEstrella *datos){

    do{
     datos=(TEstrella *)malloc(MAX);
    printf("ascensionRecta: ");
    scanf("%lf", &datos->ascensionRecta);
    printf("declinacion: ");
    scanf("%lf", &datos->declinacion);
    printf("distancia: ");
    scanf("%lf", &datos->distancia);
    printf("masa: ");
    scanf("%lf", &datos->masa);
    printf("edad: ");
    scanf("%lf", &datos->edad);
    printf("brillo: ");
    scanf("%lf", &datos->brillo);
    }
    while(1);
}

int  main (int argc, char *argv[]){
 
    FILE *pf;

    struct TEstrella *estrella;

    rellenaDatos(estrella);

    if ( !(pf = fopen(file, "w")) ){
        fprintf(stderr, "Error al abrir el archivo");
    }
    fwrite(estrella, sizeof(struct TEstrella), 1, pf);

    free((void *)estrella); 

    return EXIT_SUCCESS;
}
