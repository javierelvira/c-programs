#ifndef __EXAMEN_H_
#define __EXAMEN_H_

#define N 0x200

struct TEstrella {
    double ascensionRecta;
    double declinacion;
    double distancia;
    double diametro;
    double masa;
    double edad;
    double brillo;
    //enum brillo[7] {0,1,2,3,4,5,6};
};

struct Tpila {
    struct TEstrella * data[N];
    int cima;
};
#endif