#include <stdio.h>
#include <stdlib.h>

#define MAX 0x15
#define N 0x200

struct Alumno{

    char nombre[MAX];
    char apellido1[MAX];
    char apellido2[MAX];
    int notaMatematicas;
    int notaLengua;
    int notaHistoria;
    char cambio[MAX];
    void (*Test)();
};

void test1(){
    printf("helloworld\n");
}

void test2(){
    printf("helloworld2\n");
}

void rellenar(struct Alumno *a){

    printf("Nombre: ");
    scanf("%s", a->nombre);
    printf("Apellido1: ");
    scanf("%s", a->apellido1);
    printf("Apellido2: ");
    scanf("%s", a->apellido2);
    printf("Nota de matemáticas: ");
    scanf("%i", &a->notaMatematicas);
    printf("Nota de Lengua: ");
    scanf("%i", &a->notaLengua);
    printf("Nota de Historia: ");
    scanf("%i", &a->notaHistoria);

    printf("\n");
    printf("Nombre: %s\nApellido1: %s\nApellido2: %s\nNota de Matemáticas: %i\nNota de Lengua: %i\nNota de Historia: %i\n", a->nombre, a->apellido1, a->apellido2, a->notaMatematicas, a->notaLengua, a->notaHistoria);
    printf("\n");

    int funcion;
    printf("Indique funcion(1,2): ");
    scanf("%i",&funcion);
    a->Test =  (funcion == 1) ? &test1: &test2;
    a->Test();

}

void modificar(struct Alumno *a){
int campo;
int cnt = 0;
    //scanf("%s", a->cambio);
    do{
    printf("Indique el número del campo que desea cambiar:\n1-> nombre, 2-> apellido1, 3-> apellido2, 4-> notaMatematicas, 5-> notaLengua, 6-> notaHistoria, 7-> Salir\n");
    scanf("%i",&campo);
        if(campo==1){
            scanf("%s", a->nombre);
            continue;
        }
        else if(campo==2){
            scanf("%s", a->apellido1);
            continue;
        }
        else if(campo==3){
            scanf("%s", a->apellido2);
            continue;
        }
        else if(campo==4){
            scanf("%d", &a->notaMatematicas);
            continue;
        }
        else if(campo==5){
            scanf("%d", &a->notaLengua);
            continue;
        }
        else if(campo==6){
            scanf("%d", &a->notaHistoria);
            continue;
        }
        else{
            printf("Salir\n");
            break;
        }
    cnt++;
    }while(cnt<N);

    printf("\n");
    printf("Cambios: \n");
    printf("Nombre: %s\nApellido1: %s\nApellido2: %s\nNota de Matemáticas: %i\nNota de Lengua: %i\nNota de Historia: %i\n", a->nombre, a->apellido1, a->apellido2, a->notaMatematicas, a->notaLengua, a->notaHistoria);
    printf("\n");

}

int main(int argc, char *argv[]){

    struct Alumno pila;

    rellenar(&pila);
    modificar(&pila);

    return EXIT_SUCCESS;
}





