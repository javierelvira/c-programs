#include <stdio.h>
#include <stdlib.h>

int main(){

    int A[] = {5,20,3,9,2,13,17,9};
    int pos = -1;
    int buscado = 13;

    /*Buscar el 13 en A*/

    for (int i=0; i<sizeof(A)/sizeof(int); i++)
        if(A[i]==buscado)
          pos = i;

    if (pos >0)
    printf("He encontrado %i en la posicion %i\n", buscado,pos);
    else
        printf("No está");

     return EXIT_SUCCESS;
}
