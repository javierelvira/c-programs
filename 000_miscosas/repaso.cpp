#include <stdio.h>
#include <stdlib.h>

int main(){

    int horas, tarifa, tasa, impuestos, pagabruta, paganeta;

    printf("Dime el numero de horas trabajadas: ");
    scanf("%i", &horas);
    printf("Dime la tarifa por hora: ");
    scanf("%i", &tarifa);
    printf("Dime la tasa de impuesto: ");
    scanf("%i", &tasa);
    pagabruta=horas*tarifa;
    impuestos=pagabruta*tasa;
    paganeta=pagabruta-impuestos;

    printf("La paga neta final es: %i", paganeta);

    return EXIT_SUCCESS;
}