#include <stdio.h>
#include <stdlib.h>

int main(){

    int pelicula1, pelicula2, pelicula3, total;

    printf("Dime el precio de la primera pelicula: ");
    scanf("%i", &pelicula1);
    printf("Dime el precio de la segunda pelicula: ");
    scanf("%i", &pelicula2);
    printf("Dime el precio de la tercera pelicula: ");
    scanf("%i", &pelicula3);

    if (pelicula1>pelicula2 && pelicula2>pelicula3) {
        total = pelicula2 + pelicula3;

        if (pelicula2 > pelicula1 && pelicula1 > pelicula3) {
            total = pelicula1+pelicula3;
        }
    }
    else{
        total= pelicula2+pelicula1;
    }
    printf("El precio total seria: %i", total);

    return EXIT_SUCCESS;
}

