#include <stdio.h>
#include <stdlib.h>

int main(){

    int numero, absoluto;

    printf("Dime el numero: ");
    scanf("%i", &numero);
    if (numero>0)
        absoluto=numero;
    else
        absoluto=-numero;


    printf("El valor absoluto del numero es: %i\n", absoluto);

    return EXIT_SUCCESS;
}
