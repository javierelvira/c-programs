#include <stdio.h>
#include <stdlib.h>

int main(){

    int nota1, nota2, nota3, nota4, maxima, minima;
    double promedio;

    printf("Dime la primera nota: ");
    scanf("%i", &nota1);
    printf("Dime la segunda nota: ");
    scanf("%i", &nota2);
    printf("Dime la tercera nota: ");
    scanf("%i", &nota3);
    printf("Dime la cuarta nota: ");
    scanf("%i", &nota4);

    if (nota1>nota2 && nota2>nota3 && nota3>nota4) {
        maxima = nota1;
        minima = nota4;
        if (nota2>nota3 && nota3>nota4 && nota4>nota1) {
            maxima = nota2;
            minima = nota1;
            if (nota3>nota4 && nota4>nota1 && nota1>nota2) {
                maxima = nota3;
                minima = nota2;
            }
        }
    }
    else {
        maxima = nota4;
        minima = nota3;
    }
    promedio=((nota1+nota2+nota3+nota4)/4);
    printf("El promedio de la notas es %.2lf, la nota maxima %i y la minima %i", promedio, maxima, minima);


    return EXIT_SUCCESS;
}
